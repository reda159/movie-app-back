FROM gradle:jdk19-jammy as movie_app_back
WORKDIR /back
COPY . /back
EXPOSE 8080
CMD ["sh", "start.sh"]