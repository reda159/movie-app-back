package com.movie.app.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.movie.app.utils.enums.CONTENT_TYPE;

import java.io.Serializable;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ContentDTO implements Serializable {
    private Long id;
    private String name;
    private CONTENT_TYPE type;
    private Long movieDbId;

    public ContentDTO(Long id) {
        this.id = id;
    }

    public ContentDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CONTENT_TYPE getType() {
        return type;
    }

    public void setType(CONTENT_TYPE type) {
        this.type = type;
    }

    public Long getMovieDbId() {
        return movieDbId;
    }

    public void setMovieDbId(Long movieDbId) {
        this.movieDbId = movieDbId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContentDTO entity = (ContentDTO) o;
        return Objects.equals(this.id, entity.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "id = " + id + ", " +
                "name = " + name + ", " +
                "type = " + type.toString() + ", " +
                "movieDbId = " + movieDbId + ", " + ")";
    }
}
