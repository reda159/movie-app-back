package com.movie.app.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.movie.app.utils.enums.WATCH_STATUS;

import java.io.Serializable;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class WatchDTO implements Serializable {
    private WatchIdDTO id;
    private ContentDTO content;
    private UsersDTO user;
    private WATCH_STATUS status;

    public WatchDTO(WatchIdDTO id, ContentDTO idContent, UsersDTO idUser, WATCH_STATUS status) {
        this.id = id;
        this.content = idContent;
        this.user = idUser;
        this.status = status;
    }

    public WatchDTO() {}

    public WatchIdDTO getId() {
        return id;
    }

    public void setId(WatchIdDTO id) {
        this.id = id;
    }

    public ContentDTO getContent() {
        return content;
    }

    public void setContent(ContentDTO content) {
        this.content = content;
    }

    public UsersDTO getUser() {
        return user;
    }

    public void setUser(UsersDTO user) {
        this.user = user;
    }

    public WATCH_STATUS getStatus() {
        return status;
    }

    public void setStatus(WATCH_STATUS status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WatchDTO entity = (WatchDTO) o;
        return Objects.equals(this.id, entity.id) &&
                Objects.equals(this.content, entity.content) &&
                Objects.equals(this.user, entity.user) &&
                Objects.equals(this.status, entity.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, content, user, status);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "id = " + id + ", " +
                "content = " + content + ", " +
                "user = " + user + ", " +
                "status = " + status.toString() + ")";
    }
}
