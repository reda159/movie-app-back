package com.movie.app.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AppreciateDTO implements Serializable {
    private AppreciateIdDTO id;
    private ContentDTO content;
    private UsersDTO user;
    private Short rating;
    private String notice;

    public AppreciateDTO(AppreciateIdDTO id, ContentDTO content, UsersDTO user, Short rating, String notice) {
        this.id = id;
        this.content = content;
        this.user = user;
        this.rating = rating;
        this.notice = notice;
    }

    public AppreciateDTO() {
    }

    public AppreciateIdDTO getId() {
        return id;
    }

    public void setId(AppreciateIdDTO id) {
        this.id = id;
    }

    public ContentDTO getContent() {
        return content;
    }

    public void setContent(ContentDTO content) {
        this.content = content;
    }

    public UsersDTO getUser() {
        return user;
    }

    public void setUser(UsersDTO user) {
        this.user = user;
    }

    public Short getRating() {
        return rating;
    }

    public void setRating(Short rating) {
        this.rating = rating;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AppreciateDTO entity = (AppreciateDTO) o;
        return Objects.equals(this.id, entity.id) &&
                Objects.equals(this.content, entity.content) &&
                Objects.equals(this.user, entity.user) &&
                Objects.equals(this.rating, entity.rating) &&
                Objects.equals(this.notice, entity.notice);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, content, user, rating, notice);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "id = " + id + ", " +
                "content = " + content + ", " +
                "user = " + user + ", " +
                "rating = " + rating + ", " +
                "notice = " + notice + ")";
    }
}
