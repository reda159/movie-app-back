package com.movie.app.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AppreciateIdDTO implements Serializable {
    private Long idContent;
    private Long idUser;

    public AppreciateIdDTO(Long idContent, Long idUser) {
        this.idContent = idContent;
        this.idUser = idUser;
    }

    public Long getIdContent() {
        return idContent;
    }

    public void setIdContent(Long idContent) {
        this.idContent = idContent;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AppreciateIdDTO entity = (AppreciateIdDTO) o;
        return Objects.equals(this.idContent, entity.idContent) &&
                Objects.equals(this.idUser, entity.idUser);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idContent, idUser);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "idContent = " + idContent + ", " +
                "idUser = " + idUser + ")";
    }
}
