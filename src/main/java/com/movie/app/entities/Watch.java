package com.movie.app.entities;

import com.movie.app.utils.enums.WATCH_STATUS;
import jakarta.persistence.*;

@Entity
@Table(name = "watch")
public class Watch {
    @EmbeddedId
    private WatchId id;

    @MapsId("idContent")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_content", nullable = false)
    private Content content;

    @MapsId("idUser")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_user", nullable = false)
    private Users user;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private WATCH_STATUS status;

    public WATCH_STATUS getStatus() {
        return status;
    }

    public void setStatus(WATCH_STATUS status) {
        this.status = status;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users idUsers) {
        this.user = idUsers;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content idContent) {
        this.content = idContent;
    }

    public WatchId getId() {
        return id;
    }

    public void setId(WatchId id) {
        this.id = id;
    }
}