package com.movie.app.entities;

import jakarta.persistence.*;

@Entity
@Table(name = "appreciate")
public class Appreciate {
    @EmbeddedId
    private AppreciateId id;

    @MapsId("idContent")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_content", nullable = false)
    private Content content;

    @MapsId("idUser")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_user", nullable = false)
    private Users user;

    @Column(name = "rating", length = 50)
    private Short rating;

    @Column(name = "notice", length = 50)
    private String notice;

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public Short getRating() {
        return rating;
    }

    public void setRating(Short rating) {
        this.rating = rating;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users idUsers) {
        this.user = idUsers;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public AppreciateId getId() {
        return id;
    }

    public void setId(AppreciateId id) {
        this.id = id;
    }
}