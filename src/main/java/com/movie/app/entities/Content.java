package com.movie.app.entities;

import com.movie.app.utils.enums.CONTENT_TYPE;
import jakarta.persistence.*;

@Entity
@Table(name = "content")
public class Content {
    @Id
    @SequenceGenerator(name = "content_id_seq", sequenceName = "content_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "content_id_seq")
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
    private CONTENT_TYPE type;

    @Column(name = "moviedb_id", nullable = false)
    private Long movieDbId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CONTENT_TYPE getType() {
        return type;
    }

    public void setType(CONTENT_TYPE type) {
        this.type = type;
    }

    public Long getMovieDbId() {
        return movieDbId;
    }

    public void setMovieDbId(Long movieDbId) {
        this.movieDbId = movieDbId;
    }
}