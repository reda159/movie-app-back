package com.movie.app.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.Entity;
import org.hibernate.Hibernate;

import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class AppreciateId implements Serializable {
    private static final long serialVersionUID = -3528399290969614215L;
    @Column(name = "id_content", nullable = false)
    private Long idContent;
    @Column(name = "id_user", nullable = false)
    private Long idUser;

    public AppreciateId(Long idContent, Long idUser) {
        this.idContent = idContent;
        this.idUser = idUser;
    }

    public AppreciateId() {
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Long getIdContent() {
        return idContent;
    }

    public void setIdContent(Long idContent) {
        this.idContent = idContent;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idUser, idContent);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        AppreciateId entity = (AppreciateId) o;
        return Objects.equals(this.idUser, entity.idUser) &&
                Objects.equals(this.idContent, entity.idContent);
    }
}