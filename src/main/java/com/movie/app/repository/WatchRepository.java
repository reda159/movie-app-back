package com.movie.app.repository;

import com.movie.app.entities.Watch;
import com.movie.app.entities.WatchId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface WatchRepository extends JpaRepository<Watch, WatchId>, JpaSpecificationExecutor<Watch> {

    Watch findById_IdUserAndId_IdContent(long idUser, long idContent);

    Watch findByUser_IdAndAndContent_MovieDbId(long idUser, long movieDbId);
}