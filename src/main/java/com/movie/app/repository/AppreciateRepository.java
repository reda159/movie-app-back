package com.movie.app.repository;

import com.movie.app.entities.Appreciate;
import com.movie.app.entities.AppreciateId;
import com.movie.app.entities.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface AppreciateRepository extends JpaRepository<Appreciate, AppreciateId>, JpaSpecificationExecutor<Appreciate> {

    void deleteById_IdContentAndId_IdUser(long idContent, long idUser);

    Appreciate findById_IdContentAndId_IdUser(long idContent, long idUser);

    Appreciate findByUser_IdAndAndContent_MovieDbId(long idContent, long movieDbId);
}