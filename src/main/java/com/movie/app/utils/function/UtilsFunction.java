package com.movie.app.utils.function;

import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

public class UtilsFunction {


    /**
     * This function analyse the sort param pass in all the search method.
     * And permit to sorting by one field and to choose the direction
     * In case of the sortDirection is wrong this return a BadRequest
     * Also if the sortField is wrong this return a BadRequest
     *
     * @param sort String : this is the sort param. This contains "theFieldToSort,theSortDirection" or "theFieldToSort"
     * @return a list of Sort.Order
     */
    public static List<Sort.Order> getSorting(String sort, Class<?> object) {
        // Sorting part
        List<Sort.Order> orders = new ArrayList<>();
        // If we have a ",", that means that we sort by a field and a direction
        if (sort.contains(",")) {
            // [Field to sort] [Sort direction]
            String[] sortSplit = sort.split(",");
            if (sortSplit[1].equals("asc")) {
                orders.add(Sort.Order.asc(sortSplit[0]));
            } else if (sortSplit[1].equals("desc")) {
                orders.add(Sort.Order.desc(sortSplit[0]));
            } else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Sort direction wrong");
            }
        } else {
            // If we have only a field sort by this field in DESC order
            orders.add(Sort.Order.desc(sort));
        }

        // Check if the field is valid
        try {
            object.getDeclaredField(orders.get(0).getProperty());
        } catch (NoSuchFieldException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No such field");
        }
        return orders;
    }
}
