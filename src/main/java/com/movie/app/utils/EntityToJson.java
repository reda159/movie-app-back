package com.movie.app.utils;

import com.movie.app.dto.*;
import com.movie.app.entities.Appreciate;
import com.movie.app.entities.Content;
import com.movie.app.entities.Users;
import com.movie.app.entities.Watch;

public class EntityToJson {

    public static UsersDTO convert(Users users) {
        UsersDTO usersDTO = new UsersDTO();

        usersDTO.setId(users.getId());
        usersDTO.setFirstname(users.getFirstname());
        usersDTO.setLastname(users.getLastname());
        usersDTO.setEmail(users.getEmail());

        return usersDTO;
    }

    public static AppreciateDTO convert(Appreciate appreciate) {
        AppreciateDTO appreciateDTO = new AppreciateDTO();
        AppreciateIdDTO appreciateIdDTO = new AppreciateIdDTO(appreciate.getId().getIdContent(), appreciate.getId().getIdUser());

        appreciateDTO.setId(appreciateIdDTO);
        appreciateDTO.setContent(convert(appreciate.getContent()));
        appreciateDTO.setUser(convert(appreciate.getUser()));
        appreciateDTO.setNotice(appreciate.getNotice());
        appreciateDTO.setRating(appreciate.getRating());

        return appreciateDTO;
    }

    public static ContentDTO convert(Content content) {
        ContentDTO contentDTO = new ContentDTO();

        contentDTO.setId(content.getId());
        contentDTO.setName(content.getName());
        contentDTO.setType(content.getType());
        contentDTO.setMovieDbId(content.getMovieDbId());

        return contentDTO;
    }

    public static WatchDTO convert(Watch watch) {
        WatchDTO watchDTO = new WatchDTO();
        WatchIdDTO watchIdDTO = new WatchIdDTO(watch.getId().getIdContent(), watch.getId().getIdUser());

        watchDTO.setId(watchIdDTO);
        watchDTO.setContent(convert(watch.getContent()));
        watchDTO.setUser(convert(watch.getUser()));
        watchDTO.setStatus(watch.getStatus());

        return watchDTO;
    }
}
