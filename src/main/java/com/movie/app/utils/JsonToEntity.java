package com.movie.app.utils;

import com.movie.app.dto.*;
import com.movie.app.entities.*;
import com.movie.app.utils.enums.CONTENT_TYPE;

public class JsonToEntity {

    public static Users convert(UsersDTO usersDTO) {
        Users users = new Users();

        users.setId(usersDTO.getId());
        users.setFirstname(usersDTO.getFirstname());
        users.setLastname(usersDTO.getLastname());
        users.setEmail(usersDTO.getEmail());

        return users;
    }

    public static Appreciate convert(AppreciateDTO appreciateDTO) {
        Appreciate appreciate = new Appreciate();

        appreciate.setId(new AppreciateId(appreciateDTO.getContent().getId(), appreciateDTO.getUser().getId()));
        appreciate.setContent(convert(appreciateDTO.getContent()));
        appreciate.setUser(convert(appreciateDTO.getUser()));
        appreciate.setNotice(appreciateDTO.getNotice());
        appreciate.setRating(appreciateDTO.getRating());

        return appreciate;
    }

    public static Content convert(ContentDTO contentDTO) {
        Content content = new Content();

        content.setId(contentDTO.getId());
        content.setName(contentDTO.getName());
        if (contentDTO.getType() != null)
            content.setType(CONTENT_TYPE.valueOf(contentDTO.getType().toString()));
        content.setMovieDbId(contentDTO.getMovieDbId());

        return content;
    }

    public static Watch convert(WatchDTO watchDTO) {
        Watch watch = new Watch();

        watch.setId(new WatchId(watchDTO.getContent().getId(), watchDTO.getUser().getId()));
        watch.setContent(convert(watchDTO.getContent()));
        watch.setUser(convert(watchDTO.getUser()));
        watch.setStatus(watchDTO.getStatus());

        return watch;
    }
}
