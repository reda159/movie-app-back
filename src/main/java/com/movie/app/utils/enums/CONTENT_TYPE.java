package com.movie.app.utils.enums;

// /!\ Always add item at the end
public enum CONTENT_TYPE {
    MOVIE,
    SERIES
}
