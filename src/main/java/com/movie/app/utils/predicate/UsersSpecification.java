package com.movie.app.utils.predicate;

import com.movie.app.entities.Users;
import org.springframework.data.jpa.domain.Specification;

public class UsersSpecification {

    private static Specification<Users> hasId(Long userId) {
        return (root, query, builder) -> {
            if (userId == null) {
                return null;
            }
            return builder.equal(root.get("id"), userId);
        };
    }

    private static Specification<Users> hasEmail(String email) {
        return (root, query, builder) -> {
            if (email == null) {
                return null;
            }
            return builder.equal(root.get("email"), email);
        };
    }


    public static Specification<Users> search(Long userId, String email) {
        return Specification.where(hasId(userId)).and(hasEmail(email));
    }
}
