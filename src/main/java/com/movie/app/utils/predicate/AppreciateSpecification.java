package com.movie.app.utils.predicate;

import com.movie.app.entities.Appreciate;
import com.movie.app.entities.Watch;
import com.movie.app.utils.enums.WATCH_STATUS;
import org.springframework.data.jpa.domain.Specification;

public class AppreciateSpecification {

    private static Specification<Appreciate> hasUserId(Long userId) {
        return (root, query, builder) -> {
            if (userId == null) {
                return null;
            }
            return builder.equal(root.get("user").get("id"), userId);
        };
    }

    public static Specification<Appreciate> search(Long userId) {
        return Specification.where(hasUserId(userId));
    }
}
