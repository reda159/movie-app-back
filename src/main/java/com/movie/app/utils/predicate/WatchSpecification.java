package com.movie.app.utils.predicate;

import com.movie.app.entities.Watch;
import com.movie.app.utils.enums.WATCH_STATUS;
import org.springframework.data.jpa.domain.Specification;

public class WatchSpecification {

    private static Specification<Watch> hasUserId(Long userId) {
        return (root, query, builder) -> {
            if (userId == null) {
                return null;
            }
            return builder.equal(root.get("user").get("id"), userId);
        };
    }

    private static Specification<Watch> hasWatchStatus(WATCH_STATUS watchStatus) {
        return (root, query, builder) -> {
            if (watchStatus == null) {
                return null;
            }
            return builder.equal(root.get("status"), watchStatus);
        };
    }


    public static Specification<Watch> search(Long userId, WATCH_STATUS watchStatus) {
        return Specification.where(hasUserId(userId)).and(hasWatchStatus(watchStatus));
    }
}
