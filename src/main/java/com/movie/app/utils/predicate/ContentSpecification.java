package com.movie.app.utils.predicate;

import com.movie.app.entities.Content;
import com.movie.app.entities.Users;
import com.movie.app.utils.enums.CONTENT_TYPE;
import org.springframework.data.jpa.domain.Specification;

public class ContentSpecification {

    private static Specification<Content> hasId(Long userId) {
        return (root, query, builder) -> {
            if (userId == null) {
                return null;
            }
            return builder.equal(root.get("id"), userId);
        };
    }

    private static Specification<Content> hasName(String name) {
        return (root, query, builder) -> {
            if (name == null) {
                return null;
            }
            return builder.equal(root.get("name"), name);
        };
    }

    private static Specification<Content> hasType(CONTENT_TYPE type) {
        return (root, query, builder) -> {
            if (type == null) {
                return null;
            }
            return builder.equal(root.get("type"), type);
        };
    }

    public static Specification<Content> search(Long userId, String name) {
        return Specification.where(hasId(userId)).and(hasName(name));
    }
}
