package com.movie.app.services;

import com.movie.app.dto.AppreciateDTO;
import com.movie.app.entities.Appreciate;
import com.movie.app.entities.Content;
import com.movie.app.repository.AppreciateRepository;
import com.movie.app.repository.ContentRepository;
import com.movie.app.utils.EntityToJson;
import com.movie.app.utils.JsonToEntity;
import com.movie.app.utils.predicate.AppreciateSpecification;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AppreciateService {

    @Autowired
    private AppreciateRepository appreciateRepository;
    @Autowired
    private ContentService contentService;
    @Autowired
    private ContentRepository contentRepository;

    public Page<AppreciateDTO> search(Long userId, Pageable pageable) {
        // Get all data from database and transform entities to dto
        return appreciateRepository.findAll(AppreciateSpecification.search(userId), pageable)
                .map(EntityToJson::convert);
    }

    public Appreciate save(AppreciateDTO dto) {
        Appreciate bean = JsonToEntity.convert(dto);
        Optional<Content> content = contentRepository.findByMovieDbId(dto.getContent().getMovieDbId());
        if (content.isEmpty())
            bean.getContent().setId(contentService.save(dto.getContent()));
        else
            bean.getContent().setId(content.get().getId());
        bean = appreciateRepository.save(bean);
        return bean;
    }

    public void delete(Long idUser, Long movieDbId) {
        appreciateRepository.delete(requireOne(idUser, movieDbId));
    }

    public void update(AppreciateDTO dto) {
        Appreciate bean = requireOne(dto.getContent().getId(), dto.getUser().getId());
        BeanUtils.copyProperties(dto, bean);
        appreciateRepository.save(bean);
    }

    public AppreciateDTO getById(Long idUser, Long idContent) {
        return EntityToJson.convert(requireOne(idUser, idContent));
    }

    private Appreciate requireOne(Long idUser, Long movieDbId) {
        return appreciateRepository.findByUser_IdAndAndContent_MovieDbId(idUser, movieDbId);
    }
}
