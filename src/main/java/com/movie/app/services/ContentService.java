package com.movie.app.services;

import com.movie.app.dto.ContentDTO;
import com.movie.app.entities.Content;
import com.movie.app.repository.ContentRepository;
import com.movie.app.utils.EntityToJson;
import com.movie.app.utils.JsonToEntity;
import com.movie.app.utils.predicate.ContentSpecification;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
public class ContentService {

    @Autowired
    private ContentRepository contentRepository;

    public Page<ContentDTO> search(Long contentId, String name, Pageable pageable) {
        // Get all data from database and transform entities to dto
        return contentRepository.findAll(ContentSpecification.search(contentId, name), pageable)
                .map(EntityToJson::convert);
    }

    public Long save(ContentDTO dto) {
        Content bean = JsonToEntity.convert(dto);
        bean = contentRepository.save(bean);
        return bean.getId();
    }

    public void delete(Long id) {
        contentRepository.deleteById(id);
    }

    public void update(Long id, ContentDTO dto) {
        Content bean = requireOne(id);
        BeanUtils.copyProperties(dto, bean);
        contentRepository.save(bean);
    }

    public ContentDTO getById(Long id) {
        return EntityToJson.convert(requireOne(id));
    }

    private Content requireOne(Long id) {
        return contentRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Resource not found: " + id));
    }
}
