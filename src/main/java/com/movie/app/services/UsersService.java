package com.movie.app.services;

import com.movie.app.dto.UsersDTO;
import com.movie.app.entities.Users;
import com.movie.app.repository.UsersRepository;
import com.movie.app.utils.EntityToJson;
import com.movie.app.utils.predicate.UsersSpecification;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
public class UsersService {

    @Autowired
    private UsersRepository usersRepository;

    public Page<UsersDTO> search(Long userId, String email, Pageable pageable) {
        // Get all data from database and transform entities to dto
        return usersRepository.findAll(UsersSpecification.search(userId, email), pageable)
                .map(EntityToJson::convert);
    }

    public Long save(UsersDTO dto) {
        Users bean = new Users();
        BeanUtils.copyProperties(dto, bean);
        bean = usersRepository.save(bean);
        return bean.getId();
    }

    public void delete(Long id) {
        usersRepository.deleteById(id);
    }

    public void update(Long id, UsersDTO dto) {
        Users bean = requireOne(id);
        BeanUtils.copyProperties(dto, bean);
        usersRepository.save(bean);
    }

    public UsersDTO getById(Long id) {
        return EntityToJson.convert(requireOne(id));
    }

    private Users requireOne(Long id) {
        return usersRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Resource not found: " + id));
    }
}
