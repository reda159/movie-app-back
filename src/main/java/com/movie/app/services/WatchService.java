package com.movie.app.services;

import com.movie.app.dto.WatchDTO;
import com.movie.app.entities.Content;
import com.movie.app.entities.Watch;
import com.movie.app.repository.ContentRepository;
import com.movie.app.repository.WatchRepository;
import com.movie.app.utils.EntityToJson;
import com.movie.app.utils.JsonToEntity;
import com.movie.app.utils.enums.WATCH_STATUS;
import com.movie.app.utils.predicate.WatchSpecification;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class WatchService {

    @Autowired
    private WatchRepository watchRepository;
    @Autowired
    private ContentService contentService;
    @Autowired
    private ContentRepository contentRepository;

    public Page<WatchDTO> search(Long userId, WATCH_STATUS watchStatus, Pageable pageable) {
        // Get all data from database and transform entities to dto
        return watchRepository.findAll(WatchSpecification.search(userId, watchStatus), pageable)
                .map(EntityToJson::convert);
    }

    public Long save(WatchDTO dto) {
        Watch bean = JsonToEntity.convert(dto);
        Optional<Content> content = contentRepository.findByMovieDbId(dto.getContent().getMovieDbId());
        if (content.isEmpty())
            bean.getContent().setId(contentService.save(dto.getContent()));
        else
            bean.getContent().setId(content.get().getId());
        bean = watchRepository.save(bean);
        return bean.getContent().getId();
    }

    public void delete(long idUser, long movieDbId) {
        watchRepository.delete(requireOne(idUser, movieDbId));
    }

    public void update(WatchDTO dto) {
        Watch bean = requireOne(dto.getUser().getId(), dto.getContent().getId());
        BeanUtils.copyProperties(dto, bean);
        watchRepository.save(bean);
    }

    public WatchDTO getById(Long idUser, Long movieDbId) {
        return EntityToJson.convert(requireOne(idUser, movieDbId));
    }

    private Watch requireOne(Long idUser, Long movieDbId) {
        return watchRepository.findByUser_IdAndAndContent_MovieDbId(idUser, movieDbId);
    }
}
