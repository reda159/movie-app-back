package com.movie.app.webservices;

import com.movie.app.dto.AppreciateDTO;
import com.movie.app.dto.UsersDTO;
import com.movie.app.entities.Appreciate;
import com.movie.app.entities.Users;
import com.movie.app.services.AppreciateService;
import com.movie.app.utils.enums.WATCH_STATUS;
import com.movie.app.utils.function.UtilsFunction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Validated
@RestController
@RequestMapping("/appreciate")
public class AppreciateController {

    @Autowired
    private AppreciateService appreciateService;

    @GetMapping
    public Page<AppreciateDTO> search(
            @RequestParam(defaultValue = "0") int offset,
            @RequestParam(defaultValue = "50") int pageSize,
            @RequestParam(defaultValue = "id,desc") String sort,
            @RequestParam(name = "userId", required = false) Long userId
    ) {
        return appreciateService.search(userId,
                PageRequest.of(offset,
                        pageSize,
                        Sort.by(UtilsFunction.getSorting(sort, Appreciate.class))
                ));
    }

    @PostMapping
    public String save(@RequestBody AppreciateDTO dto) {
        return appreciateService.save(dto).toString();
    }

    @DeleteMapping("/{idUser}/{movieDbId}")
    public void delete(@PathVariable("idUser") Long idUser,
                       @PathVariable("movieDbId") Long movieDbId) {
        appreciateService.delete(idUser, movieDbId);
    }

    @PutMapping()
    public void update(@RequestBody AppreciateDTO dto) {
        appreciateService.update(dto);
    }

    @GetMapping("/{idUser}/{movieDbId}")
    public AppreciateDTO getById(@PathVariable("idUser") Long idUser,
                                 @PathVariable("movieDbId") Long movieDbId) {
        return appreciateService.getById(idUser, movieDbId);
    }
}
