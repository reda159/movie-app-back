package com.movie.app.webservices;

import com.movie.app.dto.ContentDTO;
import com.movie.app.entities.Content;
import com.movie.app.services.ContentService;
import com.movie.app.utils.function.UtilsFunction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Validated
@RestController
@RequestMapping("/content")
public class ContentController {

    @Autowired
    private ContentService contentService;

    @GetMapping
    public Page<ContentDTO> search(
            @RequestParam(name = "contentId", required = false) Long id,
            @RequestParam(name = "name", required = false) String name,
            @RequestParam(defaultValue = "0") int offset,
            @RequestParam(defaultValue = "50") int pageSize,
            @RequestParam(defaultValue = "id,desc") String sort
    ) {
        return contentService.search(id, name,
                PageRequest.of(offset,
                        pageSize,
                        Sort.by(UtilsFunction.getSorting(sort, Content.class))
                ));
    }

    @PostMapping
    public String save(@RequestBody ContentDTO dto) {
        return contentService.save(dto).toString();
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        contentService.delete(id);
    }

    @PutMapping("/{id}")
    public void update(@PathVariable("id") Long id,
                       @RequestBody ContentDTO dto) {
        contentService.update(id, dto);
    }

    @GetMapping("/{id}")
    public ContentDTO getById(@PathVariable("id") Long id) {
        return contentService.getById(id);
    }
}
