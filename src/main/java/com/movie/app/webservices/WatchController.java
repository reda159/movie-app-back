package com.movie.app.webservices;

import com.movie.app.dto.WatchDTO;
import com.movie.app.entities.Watch;
import com.movie.app.services.WatchService;
import com.movie.app.utils.enums.WATCH_STATUS;
import com.movie.app.utils.function.UtilsFunction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Validated
@RestController
@RequestMapping("/watch")
public class WatchController {

    @Autowired
    private WatchService watchService;

    @GetMapping
    public Page<WatchDTO> search(
            @RequestParam(defaultValue = "0") int offset,
            @RequestParam(defaultValue = "50") int pageSize,
            @RequestParam(defaultValue = "id,desc") String sort,
            @RequestParam(name = "userId", required = false) Long userId,
            @RequestParam(name = "watchStatus", required = false) WATCH_STATUS watchStatus
            ) {
        return watchService.search(userId, watchStatus,
                PageRequest.of(offset,
                        pageSize,
                        Sort.by(UtilsFunction.getSorting(sort, Watch.class))
                ));
    }

    @PostMapping
    public String save(@RequestBody WatchDTO dto) {
        return watchService.save(dto).toString();
    }

    @DeleteMapping("/{idUser}/{movieDbId}")
    public void delete(@PathVariable("idUser") Long idUser,
                       @PathVariable("movieDbId") Long movieDbId
    ) {
        watchService.delete(idUser, movieDbId);
    }

    @PutMapping
    public void update(@RequestBody WatchDTO dto) {
        watchService.update(dto);
    }

    @GetMapping("/{idUser}/{movieDbId}")
    public WatchDTO getById(@PathVariable("idUser") Long idUser,
                            @PathVariable("movieDbId") Long movieDbId
    ) {
        return watchService.getById(idUser, movieDbId);
    }
}
