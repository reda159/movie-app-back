package com.movie.app.webservices;

import com.movie.app.dto.UsersDTO;
import com.movie.app.entities.Users;
import com.movie.app.services.UsersService;
import com.movie.app.utils.function.UtilsFunction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Validated
@RestController
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UsersService usersService;

    @GetMapping
    public Page<UsersDTO> search(
            @RequestParam(name = "userId", required = false) Long id,
            @RequestParam(name = "email", required = false) String email,
            @RequestParam(defaultValue = "0") int offset,
            @RequestParam(defaultValue = "50") int pageSize,
            @RequestParam(defaultValue = "id,desc") String sort
    ) {
        return usersService.search(id, email,
                PageRequest.of(offset,
                        pageSize,
                        Sort.by(UtilsFunction.getSorting(sort, Users.class))
                ));
    }

    @PostMapping
    public String save(@RequestBody UsersDTO dto) {
        return usersService.save(dto).toString();
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        usersService.delete(id);
    }

    @PutMapping("/{id}")
    public void update(@PathVariable("id") Long id,
                       @RequestBody UsersDTO dto) {
        usersService.update(id, dto);
    }

    @GetMapping("/{id}")
    public UsersDTO getById(@PathVariable("id") Long id) {
        return usersService.getById(id);
    }
}
